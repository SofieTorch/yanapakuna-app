﻿using System;

namespace DAO.Model
{
    public class Event : Happening
    {
        public DateTime StartDate { get; set; }
        public DateTime FinishDate { get; set; }
    }
}