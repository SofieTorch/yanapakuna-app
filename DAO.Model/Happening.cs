﻿namespace DAO.Model
{
    public abstract class Happening: BaseAudit
    {
        public short HappeningId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public Happening() { }
    }
}