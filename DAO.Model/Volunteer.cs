﻿namespace DAO.Model
{
    public class Volunteer : Donor
    {
        public string PaternalSurname { get; set; }
        public string MaternalSurname { get; set; }
        public byte VolunteeringStatus { get; set; }

        public Volunteer() { }

        public Volunteer(Donor donorBase)
        {
            Name = donorBase.Name;
            NitOrCi = donorBase.NitOrCi;
            ModifiedBy = donorBase.ModifiedBy;
        }
    }
}