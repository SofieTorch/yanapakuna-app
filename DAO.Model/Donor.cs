﻿using System;

namespace DAO.Model
{
    public abstract class Donor : BaseAudit
    {
        #region Properties

        public short DonorId { get; set; }
        public string Name { get; set; }
        public string NitOrCi { get; set; }

        #endregion

        #region Constructors

        public Donor() { }
        
        #endregion
    }
}