﻿using System;

namespace DAO.Model
{
    public class BaseAudit
    {
        #region Properties
        public DateTime CreationDate { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public short ModifiedBy { get; set; }
        public byte RecordStatus { get; set; }
        
        #endregion

        #region Constructors

        public BaseAudit() { }
        
        /// <summary>
        /// Constructor for queries of type SELECT
        /// </summary>
        /// <param name="creationDate"></param>
        /// <param name="lastUpdateDate"></param>
        /// <param name="modifiedBy"></param>
        /// <param name="recordStatus"></param>
        public BaseAudit(
            DateTime creationDate, DateTime lastUpdateDate,
            short modifiedBy, byte recordStatus)
        {
            CreationDate = creationDate;
            LastUpdateDate = lastUpdateDate;
            ModifiedBy = modifiedBy;
            RecordStatus = recordStatus;
        }

        /// <summary>
        /// Constructor for queries of type INSERT
        /// </summary>
        /// <param name="modifiedBy"></param>
        public BaseAudit(short modifiedBy)
        {
            ModifiedBy = modifiedBy;
        }

        #endregion
    }
}