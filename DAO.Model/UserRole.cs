﻿namespace DAO.Model
{
    public enum UserRole
    {
        adminDonaciones,
        contador,
        superUsuario
    }
}