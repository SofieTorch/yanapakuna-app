﻿namespace DAO.Model
{
    public class ProjectImage : BaseAudit
    {
        public short ImageId { get; set; }
        public string Path { get; set; }
        public short ProjectId { get; set; }

        public ProjectImage() { }

        public ProjectImage(string path, short projectId)
        {
            Path = path;
            ProjectId = projectId;
        }

        public ProjectImage(short imageId, string path)
        {
            ImageId = imageId;
            Path = path;
        }
    }
}