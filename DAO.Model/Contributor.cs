﻿namespace DAO.Model
{
    public class Contributor : Donor
    {
        public string PaternalSurname { get; set; }
        public string MaternalSurname { get; set; }

        public Contributor() { }
        
        public Contributor(Donor donorBase)
        {
            Name = donorBase.Name;
            NitOrCi = donorBase.NitOrCi;
            ModifiedBy = donorBase.ModifiedBy;
        }
    }
}