﻿using System;

namespace DAO.Model
{
    public class Company : Donor
    {
        #region Properties
        public string Description { get; set; }
        public char AllianceType { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public byte AllianceStatus { get; set; }
        
        #endregion

        #region Constructors
        public Company() { }
        
        #endregion
    }
}