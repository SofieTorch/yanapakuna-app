﻿namespace DAO.Model
{
    public class User : BaseAudit
    {
        public short UserId { get; set; }
        public string UserName { get; set; }
        public string Role { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Ci { get; set; }
        public byte PaswordUpdateRequired { get; set; }
        public UserRole? UserRole { get; set; }

        public User() { }

        /// <summary>
        /// Constructor to use when creating a new user
        /// </summary>
        /// <param name="modifiedBy"></param>
        /// <param name="userId"></param>
        /// <param name="userName"></param>
        /// <param name="role"></param>
        /// <param name="email"></param>
        /// <param name="name"></param>
        public User(short modifiedBy, short userId, string userName, UserRole role, string email, string name) : base(modifiedBy)
        {
            UserId = userId;
            UserName = userName;
            UserRole = role;
            Email = email;
            Name = name;
        }
        
        /// <summary>
        /// Constructor to use when updating an userName
        /// </summary>
        /// <param name="modifiedBy"></param>
        /// <param name="userId"></param>
        /// <param name="role"></param>
        /// <param name="email"></param>
        public User(short modifiedBy, short userId, string userName, string role, string email, string name, string ci) : base(modifiedBy)
        {
            UserId = userId;
            UserName = userName;
            Role = role;
            Email = email;
            Name = name;
            Ci = ci;
        }

        /// <summary>
        /// Contructor to use when a user modifies it self user account
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userName"></param>
        /// <param name="role"></param>
        /// <param name="email"></param>
        public User(short userId, string userName, UserRole? role, string email)
        {
            UserId = userId;
            UserName = userName;
            UserRole = role;
            Email = email;
        }

        /// <summary>
        /// Contructor to use when a user modifies it self user account
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userName"></param>
        /// <param name="role"></param>
        /// <param name="email"></param>
        /// <param name="paswordUpdateRequired"></param>
        public User(short userId, string userName, UserRole role, string email, byte paswordUpdateRequired)
        {
            UserId = userId;
            UserName = userName;
            UserRole = role;
            Email = email;
            PaswordUpdateRequired = paswordUpdateRequired;
        }
    }
}