﻿using System.Windows;
using System.Windows.Controls;
using DAO.Implementation.Services;
using DAO.Model;
using YanapakunaApp.Modules.Authentication;

namespace YanapakunaApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            switch (AuthService.CurrentUserRole)
            {
                case UserRole.adminDonaciones:
                    BtnCountableAccounts.Visibility = Visibility.Collapsed;
                    BtnUsersMng.Visibility = Visibility.Collapsed;
                    break;
                case UserRole.contador:
                    BtnUsersMng.Visibility = Visibility.Collapsed;
                    break;
                case UserRole.superUsuario:
                    break;
            }
            
        }

        private void OnClickOpenConfigMenu(object sender, RoutedEventArgs e)
        {
            PopupConfig.IsOpen = !PopupConfig.IsOpen;
        }

        private void GoToUsersManagement_OnClick(object sender, RoutedEventArgs e)
        {
            // TODO: Arreglar el problema con el menu cuando se ingresa a esta vista
            ContentControl.ContentTemplate = (DataTemplate) FindResource("UsersViewTemplate");
            EventManager.RegisterClassHandler(typeof(ListBox),
                ListBoxItem.MouseLeftButtonDownEvent,
                new RoutedEventHandler(this.EnableMenu_OnClick));
        }

        private void GoToPersonalAccount_OnClick(object sender, RoutedEventArgs e)
        {
            ContentControl.ContentTemplate = (DataTemplate) FindResource("PersonalUserAccountTemplate");
            EventManager.RegisterClassHandler(typeof(ListBox),
                ListBoxItem.MouseLeftButtonDownEvent,
                new RoutedEventHandler(EnableMenu_OnClick));
        }

        private void EnableMenu_OnClick(object sender, RoutedEventArgs e)
        {
            LocalValueEnumerator locallySetProperties = ContentControl.GetLocalValueEnumerator();
            while (locallySetProperties.MoveNext())
            {
                DependencyProperty propertyToClear = locallySetProperties.Current.Property;
                if (!propertyToClear.ReadOnly) { ContentControl.ClearValue(propertyToClear); }
            }
            ContentControl.Style = (Style) FindResource("NavigationContentStyle");
        }

        private void LogOut_OnClick(object sender, RoutedEventArgs e)
        {
            AuthService authService = new AuthService();
            authService.LogOut();
            LogInWindow logInWindow = new LogInWindow();
            logInWindow.Show();
            this.Close();
        }
    }
}