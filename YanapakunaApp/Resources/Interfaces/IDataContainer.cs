﻿using YanapakunaApp.Resources;

namespace YanapakunaApp.Resources.Interfaces
{
    public interface IDataContainer
    {
        event RefreshData RefreshEvent;
        void RefreshUserData();
    }
}