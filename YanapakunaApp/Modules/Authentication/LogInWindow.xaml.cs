﻿using System;
using System.Windows;
using DAO.Implementation.Services;
using DAO.Model;

namespace YanapakunaApp.Modules.Authentication
{
    public partial class LogInWindow : Window
    {
        private AuthService _authService;
        
        public LogInWindow()
        {
            InitializeComponent();
            TitleBar.BtnMaximize.Visibility = Visibility.Collapsed;
            TitleBar.BtnRestore.Visibility = Visibility.Collapsed;
        }

        private void LogIn_OnClick(object sender, RoutedEventArgs e)
        {
            // TODO: Agregar validacion de campos
            try
            {
                _authService = new AuthService();
                User currentUser = _authService.LogIn(TxbUserName.Text, PsbPassword.Password, true);
                if (currentUser != null)
                {
                    if (currentUser.PaswordUpdateRequired == 1)
                    {
                        UpdatePasswordWindow window = new UpdatePasswordWindow("logIn");
                        window.ShowDialog();   
                    }
                    else
                    {
                        MainWindow window = new MainWindow();
                        window.Show();
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Nombre de usuario y/o contraseña incorrecta");
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
    }
}