﻿using System;
using System.Windows;
using System.Windows.Controls;
using DAO.Implementation.TablesManipulation;
using DAO.Implementation.Services;
using DAO.Model;

namespace YanapakunaApp.Modules.Authentication
{
    public partial class UserPersonalAccountView : UserControl
    {
        private UserImpl _implUser;
        private User _user;
        public UserPersonalAccountView()
        {
            _implUser = new UserImpl();
            InitializeComponent();
            _user = new User(
                AuthService.CurrentUserId,
                AuthService.CurrentUserName,
                AuthService.CurrentUserRole,
                AuthService.CurrentUserEmail
            );

            TxbUserName.Text = _user.UserName;
            TxbEmail.Text = _user.Email;
            LblRole.Content = _user.UserRole;
        }

        private void EnableEmailEditing_OnClick(object sender, RoutedEventArgs e)
        {
            TxbEmail.IsEnabled = true;
            BtnSaveChanges.Visibility = Visibility.Visible;
        }

        private void EnableUserNameEditing_OnClick(object sender, RoutedEventArgs e)
        {
            TxbUserName.IsEnabled = true;
            BtnSaveChanges.Visibility = Visibility.Visible;
        }

        private void SaveChanges_OnClick(object sender, RoutedEventArgs e)
        {
            _user.UserName = TxbUserName.Text;
            _user.Email = TxbEmail.Text;
            _user.ModifiedBy = AuthService.CurrentUserId;
            
            try
            {
                int res = _implUser.Update(_user);
                TxbUserName.IsEnabled = false;
                TxbEmail.IsEnabled = false;
                if (res == 1)
                {
                    MessageBox.Show("Tus datos se actualizaron con éxito.");
                    AuthService.CurrentUserName = _user.UserName;
                    AuthService.CurrentUserEmail = _user.Email;
                }
                else
                {
                    MessageBox.Show("No se han podido actualizar tus datos, inténtalo de nuevo.");
                    TxbUserName.Text = AuthService.CurrentUserName;
                    TxbEmail.Text = AuthService.CurrentUserEmail;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un problema: ", ex.Message);
            }
        }

        private void ChangePassword_OnClick(object sender, RoutedEventArgs e)
        {
            UpdatePasswordWindow window = new UpdatePasswordWindow("passwordChange", this);
            window.ShowDialog();
        }

        public void RefreshUserData()
        {
            LogInWindow window = new LogInWindow();
            window.Show();
            Window.GetWindow(this)?.Close();
        }
    }
}