﻿using System.Windows;
using System.Windows.Media;
using DAO.Implementation.Services;
using DAO.Model;

namespace YanapakunaApp.Modules.Authentication
{
    public partial class UpdatePasswordWindow : Window
    {
        private AuthService _authService;
        private UserPersonalAccountView _userAccountView;
        private string _type;
        public UpdatePasswordWindow(string type, UserPersonalAccountView userAccountView = null)
        {
            _userAccountView = userAccountView;
            _authService = new AuthService();
            _type = type;
            InitializeComponent();

            switch (_type)
            {
                case "logIn":
                    LblCurrentPass.Visibility = Visibility.Collapsed;
                    CurrentPass.Visibility = Visibility.Collapsed;
                    LblTitle.Content = "Actualización de contraseña requerida";
                    break;
                case "passwordChange":
                    LblTitle.Content = "Actualización de contraseña";
                    break;
            }
        }

        private void UpdatePassword_OnClick(object sender, RoutedEventArgs e)
        {
            if (_type.Equals("passwordChange"))
                VerifyPasswordChange();
            else
                PasswordUpdateFromLogInValidation();                
        }

        private void PasswordUpdateFromLogInValidation()
        {
            if (Pass1.Password.Trim().Equals("") || Pass2.Password.Trim().Equals(""))
            {
                TblMessage.Visibility = Visibility.Visible;
                TblMessage.Text = "No debe dejar los campos vacíos, por favor actualice su contraseña.";
            }
            else if (!Pass1.Password.Equals(Pass2.Password))
            {
                TblMessage.Visibility = Visibility.Visible;
                TblMessage.Text = "Las contraseñas no coinciden, verifíquelas de nuevo.";
            }
            else
            {
                _authService = new AuthService();
                if (_type.Equals("logIn"))
                {
                    int res = _authService.UpdatePassword(Pass1.Password);
                    if (res > 0)
                    {
                        MessageBox.Show("Contraseña actualizada con éxito, inicie sesión nuevamente.");
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("No se ha podido actualizar la contraseña, por favor inténtelo de nuevo.");
                    }
                }
            }
        }

        private void VerifyPasswordChange()
        {
            if (CurrentPass.Password.Trim().Equals("") || Pass1.Password.Trim().Equals("") || Pass2.Password.Trim().Equals(""))
            {
                TblMessage.Visibility = Visibility.Visible;
                TblMessage.Foreground = (Brush) FindResource("BrWhite");
                TblMessage.Text = "No debe dejar los campos vacíos, por favor actualice su contraseña.";
            }
            else if (!Pass1.Password.Equals(Pass2.Password))
            {
                TblMessage.Visibility = Visibility.Visible;
                TblMessage.Text = "Las contraseñas no coinciden, verifíquelas de nuevo.";
            }
            else
            {
                User currentUserValidation = _authService.LogIn(AuthService.CurrentUserName, CurrentPass.Password, false);
                if (currentUserValidation != null)
                {
                    int res = _authService.UpdatePassword(Pass1.Password);
                    if (res > 0)
                    {
                        MessageBox.Show("Contraseña actualizada con éxito, inicie sesión nuevamente.");
                        _userAccountView.RefreshUserData();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("No se ha podido actualizar la contraseña, por favor inténtelo de nuevo.");
                    }
                }
                else
                {
                    TblMessage.Text = "La contraseña que ingresó no coincide con la actual.";
                }
            }
        }

        private void CloseWindow_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}