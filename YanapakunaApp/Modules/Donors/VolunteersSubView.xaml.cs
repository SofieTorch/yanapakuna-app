﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DAO.Implementation;
using DAO.Implementation.Services;
using DAO.Implementation.TablesManipulation;
using DAO.Model;
using YanapakunaApp.Resources;
using YanapakunaApp.Resources.Interfaces;

namespace YanapakunaApp.Modules.Donors
{
    public partial class VolunteersSubView : UserControl, IDataContainer
    {
        private VolunteerImpl _implVolunteer;
        public event RefreshData RefreshEvent;

        public VolunteersSubView()
        {
            _implVolunteer = new VolunteerImpl();
            RefreshEvent += LoadDataGrid;
            InitializeComponent();
        }

        private void LoadDataGrid()
        {
            try
            {
                DgVolunteers.ItemsSource = null;
                DgVolunteers.ItemsSource = _implVolunteer.Select().DefaultView;
                DgVolunteers.Columns[0].Visibility = Visibility.Collapsed;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        
        private void AddNewVolunteer_OnClick(object sender, RoutedEventArgs e)
        {
            AddDonorWindow window = new AddDonorWindow("voluntario", this);
            window.ShowDialog();
        }
        
        private void VolunteersSubView_OnLoaded(object sender, RoutedEventArgs e)
        {
            LoadDataGrid();
            Style rowStyle = new Style(typeof(DataGridRow));
            rowStyle.Setters.Add(new EventSetter(DataGridRow.MouseDoubleClickEvent,
                new MouseButtonEventHandler(OpenDetails_OnHandler)));
            DgVolunteers.RowStyle = rowStyle;
        }

        private void OpenDetails_OnHandler(object sender, MouseButtonEventArgs e)
        {
            Volunteer selectedVolunteer = GetSelectedVolunteer();
            VolunteerDetailsWindow detailsWindow = new VolunteerDetailsWindow(selectedVolunteer, this);
            detailsWindow.ShowDialog();
        }
        
        public void RefreshUserData()
        {
            RefreshEvent?.Invoke();
        }

        private Volunteer GetSelectedVolunteer()
        {
            DataRowView row = (DataRowView) DgVolunteers.SelectedItems[0];
            Volunteer selectedVolunteer = new Volunteer();
            selectedVolunteer.DonorId = Int16.Parse(row[0].ToString());
            selectedVolunteer.Name = row[1].ToString();
            selectedVolunteer.PaternalSurname = row[2].ToString();
            selectedVolunteer.MaternalSurname = row[3].ToString();
            selectedVolunteer.NitOrCi = row[4].ToString();
            selectedVolunteer.VolunteeringStatus = Byte.Parse(row[5].ToString().Equals("Activo")
                ? 1.ToString()
                : 0.ToString());
            selectedVolunteer.ModifiedBy = AuthService.CurrentUserId;

            return selectedVolunteer;
        }

        private void Search_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (SearchBar.Text.Length > 0)
            {
                try
                {
                    DgVolunteers.ItemsSource = null;
                    DgVolunteers.ItemsSource = _implVolunteer.Search(SearchBar.Text.ToLower()).DefaultView;
                    DgVolunteers.Columns[0].Visibility = Visibility.Collapsed;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else LoadDataGrid();
        }
    }
}