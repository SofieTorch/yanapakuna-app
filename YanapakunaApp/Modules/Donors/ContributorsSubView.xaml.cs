﻿using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DAO.Implementation.Services;
using DAO.Implementation.TablesManipulation;
using DAO.Model;
using YanapakunaApp.Resources;
using YanapakunaApp.Resources.Interfaces;

namespace YanapakunaApp.Modules.Donors
{
    public partial class ContributorsSubView : UserControl, IDataContainer
    {
        private ContributorImpl implContributor;
        private Donor donor;
        public event RefreshData RefreshEvent;
        
        public ContributorsSubView()
        {
            implContributor = new ContributorImpl();
            RefreshEvent += LoadDataGrid;
            InitializeComponent();
        }
        
        public void LoadDataGrid()
        {
            try
            {
                DgContrbutors.ItemsSource = null;
                DgContrbutors.ItemsSource = implContributor.Select().DefaultView;
                DgContrbutors.Columns[0].Visibility = Visibility.Collapsed;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        
        private void AddNewContributor_OnClick(object sender, RoutedEventArgs e)
        {
            AddDonorWindow window = new AddDonorWindow("contribuidor", this);
            window.ShowDialog();
        }

        private void ContributorsSubView_OnLoaded(object sender, RoutedEventArgs e)
        {
            LoadDataGrid();
            Style rowStyle = new Style(typeof(DataGridRow));
            rowStyle.Setters.Add(new EventSetter(DataGridRow.MouseDoubleClickEvent,
                new MouseButtonEventHandler(OpenDetails_OnHandler)));
            DgContrbutors.RowStyle = rowStyle;
        }
        
        private void OpenDetails_OnHandler(object sender, MouseButtonEventArgs e)
        {
            Contributor selectedContributor = GetSelectedContributor();
            ContributorDetailsWindow detailsWindow = new ContributorDetailsWindow(this, selectedContributor);
            detailsWindow.ShowDialog();
        }
        
        private Contributor GetSelectedContributor()
        {
            DataRowView row = (DataRowView) DgContrbutors.SelectedItems[0];
            Contributor selectedContributor = new Contributor();
            selectedContributor.DonorId = Int16.Parse(row[0].ToString());
            selectedContributor.Name = row[1].ToString();
            selectedContributor.PaternalSurname = row[2].ToString();
            selectedContributor.MaternalSurname = row[3].ToString();
            selectedContributor.NitOrCi = row[4].ToString();
            selectedContributor.ModifiedBy = AuthService.CurrentUserId;

            return selectedContributor;
        }
        
        public void RefreshUserData()
        {
            if (RefreshEvent != null) RefreshEvent.Invoke();
        }

        private void Search_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (SearchBar.Text.Length > 0)
            {
                try
                {
                    DgContrbutors.ItemsSource = null;
                    DgContrbutors.ItemsSource = implContributor.Search(SearchBar.Text).DefaultView;
                    DgContrbutors.Columns[0].Visibility = Visibility.Collapsed;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }   
            }
            else LoadDataGrid();
        }
    }
}