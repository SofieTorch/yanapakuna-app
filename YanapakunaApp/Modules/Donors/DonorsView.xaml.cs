﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace YanapakunaApp.Modules.Donors
{
    public partial class DonorsView : UserControl
    {
        public DonorsView()
        {
            InitializeComponent();
            GridContent.Children.Add(new CompaniesSubView());
        }
        private void OnClickCompanies(object sender, RoutedEventArgs e)
        {
            GridContent.Children.Clear();
            UserControl content = null;
            
            switch (((Button)sender).Name)
            {
                case "BtnCompanies":
                    BtnCompanies.Background = this.FindResource("BrWhite") as Brush;
                    BtnVolunteers.Background = this.FindResource("BrTransparent") as Brush;
                    BtnContributors.Background = this.FindResource("BrTransparent") as Brush;
                    content = new CompaniesSubView();
                    break;
                case "BtnVolunteers":
                    BtnVolunteers.Background = this.FindResource("BrWhite") as Brush;
                    BtnCompanies.Background = this.FindResource("BrTransparent") as Brush;
                    BtnContributors.Background = this.FindResource("BrTransparent") as Brush;
                    content = new VolunteersSubView();
                    break;
                case "BtnContributors":
                    BtnContributors.Background = this.FindResource("BrWhite") as Brush;
                    BtnVolunteers.Background = this.FindResource("BrTransparent") as Brush;
                    BtnCompanies.Background = this.FindResource("BrTransparent") as Brush;
                    content = new ContributorsSubView();
                    break;
                default:
                    content = new UserControl();
                    break;
            }

            GridContent.Children.Add(content);
        }
    }
}