﻿using System.Windows;
using DAO.Implementation.Services;
using DAO.Implementation.TablesManipulation;
using DAO.Model;
using YanapakunaApp.Resources.Interfaces;

namespace YanapakunaApp.Modules.Donors
{
    public partial class AddCompanyWindow : Window
    {
        private CompanyImpl _implCompany;
        private IDataContainer _parentWindow;
        public AddCompanyWindow(IDataContainer parent)
        {
            _parentWindow = parent;
            InitializeComponent();
        }

        private void RegisterCompany_OnClick(object sender, RoutedEventArgs e)
        {
            Company company = new Company();
            company.Name = TxbName.Text;
            company.NitOrCi = TxbNit.Text;
            company.Description = TxbDescription.Text;
            company.AllianceType = CbxAllianceType.Text[0];
            company.ContactName = TxbContactName.Text;
            company.ContactPhone = TxbContactPhone.Text;
            company.ModifiedBy = AuthService.CurrentUserId;

            _implCompany = new CompanyImpl();
            int res = _implCompany.Insert(company);
            if (res == 0)
            {
                MessageBox.Show("Ha ocurrido un error, inténtelo de nuevo.");
            }
            else
            {
                _parentWindow.RefreshUserData();
                this.Close();
            }
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}