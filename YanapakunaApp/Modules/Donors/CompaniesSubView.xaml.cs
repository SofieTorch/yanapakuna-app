﻿using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DAO.Implementation.TablesManipulation;
using DAO.Model;
using YanapakunaApp.Resources;
using YanapakunaApp.Resources.Interfaces;

namespace YanapakunaApp.Modules.Donors
{
    public partial class CompaniesSubView : UserControl, IDataContainer
    {
        private CompanyImpl _implCompany;
        public event RefreshData RefreshEvent;
        public CompaniesSubView()
        {
            _implCompany = new CompanyImpl();
            RefreshEvent += LoadDataGrid;
            InitializeComponent();
        }

        private void AddNewCompany_OnClick(object sender, RoutedEventArgs e)
        {
            AddCompanyWindow window = new AddCompanyWindow(this);
            window.ShowDialog();
        }

        private void OpenDetails_OnHandler(object sender, MouseButtonEventArgs e)
        {
            DataRowView row = (DataRowView) DgCompanies.SelectedItems[0];
            short companyId = Int16.Parse(row[0].ToString());
            CompanyDetailsWindow window = new CompanyDetailsWindow(companyId, this);
            window.ShowDialog();
        }

        private void CompaniesSubView_OnLoaded(object sender, RoutedEventArgs e)
        {
            LoadDataGrid();
            Style rowStyle = new Style(typeof(DataGridRow));
            rowStyle.Setters.Add(new EventSetter(DataGridRow.MouseDoubleClickEvent,
                new MouseButtonEventHandler(OpenDetails_OnHandler)));
            DgCompanies.RowStyle = rowStyle;
        }

        private void LoadDataGrid()
        {
            try
            {
                DgCompanies.ItemsSource = null;
                DgCompanies.ItemsSource = _implCompany.Select().DefaultView;
                DgCompanies.Columns[0].Visibility = Visibility.Collapsed;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        
        public void RefreshUserData()
        {
            RefreshEvent?.Invoke();
        }

        private void Search_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (SearchBar.Text.Length > 0)
            {
                try
                {
                    DgCompanies.ItemsSource = null;
                    DgCompanies.ItemsSource = _implCompany.Search(SearchBar.Text).DefaultView;
                    DgCompanies.Columns[0].Visibility = Visibility.Collapsed;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }   
            }
            else LoadDataGrid();
        }
    }
}