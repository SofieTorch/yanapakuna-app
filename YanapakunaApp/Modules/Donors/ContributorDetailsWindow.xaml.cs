﻿using System.Windows;
using DAO.Implementation.Services;
using DAO.Implementation.TablesManipulation;
using DAO.Model;
using YanapakunaApp.Resources.Interfaces;
using MessageBox = HandyControl.Controls.MessageBox;

namespace YanapakunaApp.Modules.Donors
{
    public partial class ContributorDetailsWindow : Window
    {
        private IDataContainer _parentWindow;
        private Contributor _contributor;
        private ContributorImpl _implContributor;
        public ContributorDetailsWindow(IDataContainer parent, Contributor contributor)
        {
            _parentWindow = parent;
            _contributor = contributor;
            InitializeComponent();
            LoadContributorDataInView();
        }

        private void SaveChanges_OnClick(object sender, RoutedEventArgs e)
        {
            _implContributor = new ContributorImpl();
            UploadContributorData();
            int res = _implContributor.Update(_contributor);
            if (res > 0)
            {
                MessageBox.Success("Se han actualizado los datos del contribuidor.");
                _parentWindow.RefreshUserData();
                this.Close();
            }
            else
            {
                MessageBox.Error("Hubo un problema al actualizar los datos");
            }
        }

        private void DeleteContributor_OnClick(object sender, RoutedEventArgs e)
        {
            _implContributor = new ContributorImpl();
            int res = _implContributor.Delete(_contributor);
            if (res > 0)
            {
                MessageBox.Success("Se han actualizado los datos del contribuidor.");
                _parentWindow.RefreshUserData();
                this.Close();
            }
            else
            {
                MessageBox.Error("Hubo un problema al actualizar los datos");
            }
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();   
        }

        private void LoadContributorDataInView()
        {
            TxbName.Text = _contributor.Name;
            TxbPaternalSurname.Text = _contributor.PaternalSurname;
            TxbMaternalSurname.Text = _contributor.MaternalSurname;
            TxbNitOrCi.Text = _contributor.NitOrCi;
        }

        private void UploadContributorData()
        {
            _contributor.Name = TxbName.Text;
            _contributor.PaternalSurname = TxbPaternalSurname.Text;
            _contributor.MaternalSurname = TxbMaternalSurname.Text;
            _contributor.NitOrCi = TxbNitOrCi.Text;
            _contributor.ModifiedBy = AuthService.CurrentUserId;
        }
    }
}