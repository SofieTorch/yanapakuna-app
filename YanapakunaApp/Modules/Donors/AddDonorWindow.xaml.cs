﻿using System;
using System.Windows;
using DAO.Implementation;
using DAO.Implementation.Services;
using DAO.Implementation.TablesManipulation;
using DAO.Interfaces;
using DAO.Model;
using YanapakunaApp.Resources.Interfaces;
using MessageBox = HandyControl.Controls.MessageBox;

namespace YanapakunaApp.Modules.Donors
{
    public partial class AddDonorWindow : Window
    {
        private string _donorType;
        private IDataContainer _parent;
        public AddDonorWindow(string type, IDataContainer parent)
        {
            _parent = parent;
            _donorType = type;
            InitializeComponent();
            LblTitle.Content = $"Registrar nuevo {this._donorType}";
        }

        private void RegisterDonor_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                int response;
                switch (_donorType)
                {
                    case "voluntario":
                        VolunteerImpl implVolunteer = new VolunteerImpl();
                        Volunteer volunteer = GetVolunteer();
                        response = implVolunteer.Insert(volunteer);
                        break;
                    default:
                        ContributorImpl implContributor = new ContributorImpl();
                        Contributor contributor = GetContributor();
                        response = implContributor.Insert(contributor);
                        break;
                }
                
                if (response > 0)
                {
                    _parent.RefreshUserData();
                    MessageBox.Success($"{_donorType} registrado exitosamente.");
                    this.Close();
                }
                else
                {
                    MessageBox.Error("No se ha podido registrar al voluntario, por favor inténtelo de nuevo.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Error(ex.Message);
                throw;
            }
        }

        private Volunteer GetVolunteer()
        {
            Volunteer volunteer = new Volunteer();
            volunteer.PaternalSurname = TxbPaternalSurname.Text;
            volunteer.MaternalSurname = TxbMaternalSurname.Text;
            volunteer.Name = TxbName.Text;
            volunteer.NitOrCi = TxbNitOrCi.Text;
            volunteer.ModifiedBy = AuthService.CurrentUserId;
            return volunteer;
        }

        private Contributor GetContributor()
        {
            Contributor contributor = new Contributor();
            contributor.PaternalSurname = TxbPaternalSurname.Text;
            contributor.MaternalSurname = TxbMaternalSurname.Text;
            contributor.Name = TxbName.Text;
            contributor.NitOrCi = TxbNitOrCi.Text;
            contributor.ModifiedBy = AuthService.CurrentUserId;
            return contributor;   
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}