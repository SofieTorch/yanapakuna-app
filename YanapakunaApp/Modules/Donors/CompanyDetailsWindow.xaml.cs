﻿using System;
using System.Data;
using System.Windows;
using DAO.Implementation.Services;
using DAO.Implementation.TablesManipulation;
using DAO.Model;
using YanapakunaApp.Resources.Interfaces;

namespace YanapakunaApp.Modules.Donors
{
    public partial class CompanyDetailsWindow : Window
    {
        private Company _company;
        private CompanyImpl _implCompany;
        private IDataContainer _parentWindow;
        public CompanyDetailsWindow(short companyId, IDataContainer parent)
        {
            _implCompany = new CompanyImpl();
            _company = new Company();
            _parentWindow = parent;
            InitializeComponent();
            LoadCompanyData(companyId);
            LoadCompanyDataView();
        }

        private void DeleteCompany_OnClick(object sender, RoutedEventArgs e)
        {
            _company.ModifiedBy = AuthService.CurrentUserId;
            int res = _implCompany.Delete(_company);
            if (res > 0)
            {
                _parentWindow.RefreshUserData();
                this.Close();
            }
            else
            {
                MessageBox.Show("Ha ocurrido un error, intentelo de nuevo");
            }
        }

        private void SaveChanges_OnClick(object sender, RoutedEventArgs e)
        {
            UploadCompanyData();
            int res = _implCompany.Update(_company);
            if (res > 0)
            {
                _parentWindow.RefreshUserData();
                this.Close();
            }
            else
            {
                MessageBox.Show("Ha ocurrido un error, intentelo de nuevo");
            }
        }

        private void UploadCompanyData()
        {
            _company.Name = TxbName.Text;
            _company.NitOrCi = TxbNit.Text;
            _company.Description = TxbDescription.Text;
            _company.ContactName = TxbContactName.Text;
            _company.ContactPhone = TxbContactPhone.Text;
            _company.AllianceStatus = CbxAllianceActive.Text.Equals("Activa")
                ? Byte.Parse("1")
                : Byte.Parse("0");
            _company.AllianceType = CbxAllianceType.Text[0];
            _company.ModifiedBy = AuthService.CurrentUserId;
        }

        private void LoadCompanyData(short companyId)
        {
            DataTable companyTable = _implCompany.SelectCompanyById(companyId);

            _company.DonorId = Int16.Parse(companyTable.Rows[0][0].ToString());
            _company.Name = companyTable.Rows[0][1].ToString();
            _company.NitOrCi = companyTable.Rows[0][2].ToString();
            _company.Description = companyTable.Rows[0][3].ToString();
            _company.ContactName = companyTable.Rows[0][4].ToString();
            _company.ContactPhone = companyTable.Rows[0][5].ToString();
            _company.AllianceStatus = Byte.Parse(companyTable.Rows[0][6].ToString());
            _company.AllianceType = Char.Parse(companyTable.Rows[0][7].ToString());
        }

        private void LoadCompanyDataView()
        {
            TxbName.Text = _company.Name;
            TxbNit.Text = _company.NitOrCi;
            TxbDescription.Text = _company.Description;
            TxbContactName.Text = _company.ContactName;
            TxbContactPhone.Text = _company.ContactPhone;
            CbxAllianceActive.Text = _company.AllianceStatus == 1 ? "Activa" : "Inactiva";

            switch (_company.AllianceType)
            {
                case 'C':
                    CbxAllianceType.Text = "Corto plazo";
                    break;
                case 'M':
                    CbxAllianceType.Text = "Mediano plazo";
                    break;
                case 'L':
                    CbxAllianceType.Text = "Largo plazo";
                    break;
                default:
                    break;
            }
        }
    }
}