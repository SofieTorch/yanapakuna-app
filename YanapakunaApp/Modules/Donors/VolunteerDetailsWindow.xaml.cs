﻿using System;
using System.Windows;
using DAO.Implementation;
using DAO.Implementation.Services;
using DAO.Implementation.TablesManipulation;
using DAO.Model;
using YanapakunaApp.Resources.Interfaces;
using MessageBox = HandyControl.Controls.MessageBox;

namespace YanapakunaApp.Modules.Donors
{
    public partial class VolunteerDetailsWindow : Window
    {
        private IDataContainer _parent;
        private VolunteerImpl _implVolunteer;
        private Volunteer Volunteer { get; set; }
        public VolunteerDetailsWindow(Volunteer volunteer, IDataContainer parent)
        {
            _parent = parent;
            Volunteer = volunteer;
            _implVolunteer = new VolunteerImpl();
            InitializeComponent();
            LoadVolunteerDataInView();
        }

        private void SaveChanges_OnClick(object sender, RoutedEventArgs e)
        {
            Volunteer = UpdateVolunteerData();
            
            int res = _implVolunteer.Update(Volunteer);
            if (res > 0)
            {
                MessageBox.Success("Se han actualizado los datos del voluntario");
                _parent.RefreshUserData();
                this.Close();
            }
            else
            {
                MessageBox.Error("No se ha podido actualizar los datos.");
            }
        }

        private void DeleteVolunteer_OnClick(object sender, RoutedEventArgs e)
        {
            MessageBoxResult delete = MessageBox.Show("Está seguro(a) de eliminar el registro de este voluntario?",
                "Eliminar voluntario", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            
            if (delete == MessageBoxResult.Yes)
            {
                Volunteer = UpdateVolunteerData();
                int res = _implVolunteer.Delete(Volunteer);
                if (res > 0)
                {
                    _parent.RefreshUserData();
                    this.Close();
                }
                else
                {
                    MessageBox.Error("No se ha podido eliminar el registro.");
                }
            }
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private Volunteer UpdateVolunteerData()
        {
            Volunteer volunteer = new Volunteer();
            volunteer.DonorId = Volunteer.DonorId;
            volunteer.ModifiedBy = AuthService.CurrentUserId;
            volunteer.Name = TxbName.Text;
            volunteer.PaternalSurname = TxbPaternalSurname.Text;
            volunteer.MaternalSurname = TxbMaternalSurname.Text;
            volunteer.NitOrCi = TxbNitOrCi.Text;
            volunteer.VolunteeringStatus = Byte.Parse(CbxVolunteeringStatus.SelectedIndex.ToString());

            return volunteer;
        }

        private void LoadVolunteerDataInView()
        {
            TxbName.Text = Volunteer.Name;
            TxbPaternalSurname.Text = Volunteer.PaternalSurname;
            TxbMaternalSurname.Text = Volunteer.MaternalSurname;
            TxbNitOrCi.Text = Volunteer.NitOrCi;
            CbxVolunteeringStatus.SelectedIndex = Volunteer.VolunteeringStatus;
        }
    }
}