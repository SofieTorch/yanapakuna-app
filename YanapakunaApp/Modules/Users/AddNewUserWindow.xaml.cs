﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;
using DAO.Implementation;
using DAO.Implementation.Services;
using DAO.Implementation.TablesManipulation;
using DAO.Model;
using YanapakunaApp.Resources.Interfaces;

namespace YanapakunaApp.Modules.Users
{
    public partial class AddNewUserWindow : Window
    {
        private VolunteerImpl _implVolunteer;
        private UserImpl _implUser;
        private DataTable _volunteersAvailable;
        private IDataContainer _parent;
        public AddNewUserWindow(IDataContainer parent)
        {
            _parent = parent;
            _implVolunteer = new VolunteerImpl();
            _implUser = new UserImpl();
            InitializeComponent();
            LoadVolunteersWithoutUserAccount();
        }

        private void LoadVolunteersWithoutUserAccount()
        {
            _volunteersAvailable = _implVolunteer.SelectVolunteersWithoutUserAccount();
            string[] comboBoxItems = new string[_volunteersAvailable.Rows.Count + 1];
            
            for (int i = 0; i < comboBoxItems.Length - 1; i++)
            {
                comboBoxItems[i] = _volunteersAvailable.Rows[i][1].ToString();
            }

            comboBoxItems[comboBoxItems.Length - 1] = "Nuevo voluntario...";
            CbxVolunteers.ItemsSource = comboBoxItems;
        }

        private void CbxVolunteers_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CbxVolunteers.SelectedIndex == CbxVolunteers.Items.Count - 1)
            {
                TxbName.Visibility = Visibility.Visible;
                TxbLastname.Visibility = Visibility.Visible;
                TxbCi.Text = "";
            }
            else
            {
                TxbCi.Text = _volunteersAvailable.Rows[CbxVolunteers.SelectedIndex][4].ToString();
                TxbName.Visibility = Visibility.Collapsed;
                TxbLastname.Visibility = Visibility.Collapsed;
            }
        }

        private void RegisterUser_OnClick(object sender, RoutedEventArgs e)
        {
            Volunteer volunteer = new Volunteer();
            volunteer.DonorId = Int16.Parse(_volunteersAvailable.Rows[CbxVolunteers.SelectedIndex][0].ToString());
            volunteer.Name = _volunteersAvailable.Rows[CbxVolunteers.SelectedIndex][1].ToString();
            volunteer.PaternalSurname = _volunteersAvailable.Rows[CbxVolunteers.SelectedIndex][2].ToString();
            volunteer.MaternalSurname = _volunteersAvailable.Rows[CbxVolunteers.SelectedIndex][3].ToString();
            volunteer.NitOrCi = _volunteersAvailable.Rows[CbxVolunteers.SelectedIndex][4].ToString();
            volunteer.VolunteeringStatus = 1;
            volunteer.ModifiedBy = AuthService.CurrentUserId;

            string userName = GetUserName(volunteer);
            UserRole role = GetUserRole();
            
            User user = new User(
                AuthService.CurrentUserId,
                volunteer.DonorId,
                userName,
                role,
                TxbEmail.Text,
                volunteer.Name
            );
            
            int res = _implUser.Insert(user);
            int res2 = _implVolunteer.Update(volunteer);
            if (res > 0)
            {
                _parent.RefreshUserData();
                this.Close();
            }
            else
                MessageBox.Show("Ocurrio un problema, intentelo de nuevo");
        }
        
        private string GetUserName(Volunteer volunteer)
        {
            string userName = UserNameAndPasswordGenerator.GenerateUserName(
                $"{volunteer.Name} {volunteer.PaternalSurname} {volunteer.MaternalSurname}", volunteer.NitOrCi);
            SqlDataReader dr = _implUser.VerifyIfUserNameExists(userName);
            dr.Read();
            bool isUserNameValid = dr[0].ToString() == "0";

            while (!isUserNameValid)
            {
                userName = UserNameAndPasswordGenerator.GenerateUserName($"{volunteer.Name} {volunteer.PaternalSurname} {volunteer.MaternalSurname}", volunteer.NitOrCi);
                dr = _implUser.VerifyIfUserNameExists(userName);
                dr.Read();
                isUserNameValid = dr[0].ToString() == "0";
            }

            dr.Close();
            return userName;
        }

        private UserRole GetUserRole()
        {
            UserRole role;
            switch (CbxRole.Text)
            {
                case "Administrador de donaciones":
                    role = UserRole.adminDonaciones;
                    break;
                case "Contador":
                    role = UserRole.contador;
                    break;
                default:
                    role = UserRole.superUsuario;
                    break;
            }

            return role;
        }
        
        private void CancelRegister_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}