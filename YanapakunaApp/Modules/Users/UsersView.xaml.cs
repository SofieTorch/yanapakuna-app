﻿using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DAO.Implementation.Services;
using DAO.Implementation.TablesManipulation;
using DAO.Model;
using YanapakunaApp.Resources;
using YanapakunaApp.Resources.Interfaces;

namespace YanapakunaApp.Modules.Users
{
    public partial class UsersView : UserControl, IDataContainer
    {
        private UserImpl implUser;
        public event RefreshData RefreshEvent;
        public UsersView()
        {
            implUser = new UserImpl();
            RefreshEvent += LoadDataGrid;
            InitializeComponent();
        }
        
        private void LoadDataGrid()
        {
            try
            {
                DgUsersTable.ItemsSource = null;
                DgUsersTable.ItemsSource = implUser.Select().DefaultView;
                DgUsersTable.Columns[0].Visibility = Visibility.Collapsed;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void UsersView_OnLoaded(object sender, RoutedEventArgs e)
        {
            LoadDataGrid();
            Style rowStyle = new Style(typeof(DataGridRow));
            rowStyle.Setters.Add(new EventSetter(DataGridRow.MouseDoubleClickEvent,
                new MouseButtonEventHandler(OpenUserDetails_OnHandler)));
            DgUsersTable.RowStyle = rowStyle;
        }

        private void RegisterNewUser_OnClick(object sender, RoutedEventArgs e)
        {
            AddNewUserWindow window = new AddNewUserWindow(this);
            window.ShowDialog();
        }

        public void OpenUserDetails_OnHandler(object sender, MouseButtonEventArgs e)
        {
            DataRowView userRowSelected = (DataRowView) DgUsersTable.SelectedItems[0];

            User user = new User(
                AuthService.CurrentUserId,
                Int16.Parse(userRowSelected[0].ToString()),
                userRowSelected[1].ToString(),
                userRowSelected[2].ToString(),
                userRowSelected[7].ToString(),
                userRowSelected[3].ToString() + " " + userRowSelected[4].ToString() + " " + userRowSelected[5].ToString(),
                userRowSelected[6].ToString()
            );

            EditUserWindow editUserWindow = new EditUserWindow(user, this);
            editUserWindow.ShowDialog();
        }
        
        public void RefreshUserData()
        {
            if (RefreshEvent != null) RefreshEvent.Invoke();
        }
    }
}