﻿using System;
using System.Windows;
using DAO.Implementation;
using DAO.Implementation.Services;
using DAO.Implementation.TablesManipulation;
using DAO.Model;
using YanapakunaApp.Resources.Interfaces;

namespace YanapakunaApp.Modules.Users
{
    public partial class EditUserWindow : Window
    {
        private IDataContainer _parent;
        private UserImpl _implUser;
        private User _user;
        public EditUserWindow(User user, IDataContainer parent)
        {
            _parent = parent;
            _user = user;
            _implUser = new UserImpl();
            InitializeComponent();

            TxbUserName.Text = _user.UserName;
            TxbEmail.Text = _user.Email;
            switch (_user.Role)
            {
                case "Administrador de donaciones":
                    CbxRole.SelectedIndex = 0;
                    break;
                case "Contador":
                    CbxRole.SelectedIndex = 1;
                    break;
                case "Super usuario":
                    CbxRole.SelectedIndex = 2;
                    break;
            }
        }

        private void GenerateNewUserName_OnCLick(object sender, RoutedEventArgs e)
        {
            _user.UserName = UserNameAndPasswordGenerator.GenerateUserName(_user.Name, _user.Ci);

            try
            {
                int res = _implUser.RestoreUserName(_user);
                if (res == 1)
                {
                    MessageBox.Show("El nombre de usuario se ha actualizado con éxito.");
                    TxbUserName.Text = _user.UserName;
                }
                else
                {
                    MessageBox.Show("No se ha podido actualizar el nombre de usuario, por favor inténtelo de nuevo.");
                    _user.UserName = TxbUserName.Text;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un problema: " + ex.Message);
            }
        }
        
        private void GenerateNewPassword_OnCLick(object sender, RoutedEventArgs e)
        {
            try
            {
                int res = _implUser.RestorePassword(_user);
                if (res == 1)
                {
                    MessageBox.Show("La contraseña se ha actualizado con éxito.");
                }
                else
                {
                    MessageBox.Show("No se ha podido actualizar la contraseña, por favor inténtelo de nuevo.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un problema: " + ex.Message);
            }
        }
        
        private void EnableRoleEditing_OnClick(object sender, RoutedEventArgs e)
        {
            CbxRole.IsEnabled = true;
            BtnSaveChanges.IsEnabled = true;
        }
        
        private void EnableEmailEditing_OnClick(object sender, RoutedEventArgs e)
        {
            TxbEmail.IsEnabled = true;
            BtnSaveChanges.IsEnabled = true;
        }

        private void SaveChanges_OnClick(object sender, RoutedEventArgs e)
        {
            UserRole? role = null;
            switch (CbxRole.Text)
            {
                case "Administrador de donaciones":
                    role = UserRole.adminDonaciones;
                    break;
                case "Contador":
                    role = UserRole.contador;
                    break;
                case "Super usuario":
                    role = UserRole.superUsuario;
                    break;
            }
            
            _user.ModifiedBy = AuthService.CurrentUserId;
            _user.Email = TxbEmail.Text;
            _user.UserRole = role;

            try
            {
                int res = _implUser.Update(_user);
                if (res == 1)
                {
                    _parent.RefreshUserData();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("No se han podido actualizar los datos de usuario, inténtelo de nuevo.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un problema: ", ex.Message);
            }
            
            // TODO: Crear evento para refrescar la tabla de datos
        }

        private void DeleteUser_OnClick(object sender, RoutedEventArgs e)
        {
            _user.ModifiedBy = AuthService.CurrentUserId;
            try
            {
                int res = _implUser.Delete(_user);
                if (res == 1)
                {
                    MessageBox.Show("El usuario ha sido eliminado.");
                    _parent.RefreshUserData();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("No se han podido eliminar el usuario, inténtelo de nuevo.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un problema: ", ex.Message);
            }
        }
    }
}