﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media.Imaging;
using System.Xml;
using DAO.Implementation.Services;
using DAO.Implementation.TablesManipulation;
using DAO.Model;
using HandyControl.Controls;
using HandyControl.Interactivity;
using YanapakunaApp.Resources.Interfaces;
using MessageBox = HandyControl.Controls.MessageBox;
using Window = System.Windows.Window;

namespace YanapakunaApp.Modules.Happenings
{
    public partial class EditProjectWindow : Window
    {
        private IDataContainer _parentWindow;
        private ProjectImpl _implProject;
        private Project _project;
        private List<Uri> _uris;
        public EditProjectWindow(IDataContainer parent, Project project)
        {
            _parentWindow = parent;
            _project = project;
            _uris = new List<Uri>();
            InitializeComponent();
            LoadProjectDataView();
            ImageSelector.PreviewMouseUp += ImageSelector_OnMouseDown;
        }
        
        private void AddNewImageSelector()
        {
            string imageSelectorXaml = XamlWriter.Save(ImageSelector);
            StringReader stringReader = new StringReader(imageSelectorXaml);
            XmlReader xmlReader = XmlReader.Create(stringReader);
            HandyControl.Controls.ImageSelector newImageSelector =
                (HandyControl.Controls.ImageSelector) XamlReader.Load(xmlReader);
            newImageSelector.PreviewMouseUp += ImageSelector_OnMouseDown;
            ImageSelectorGrid.Children.Add(newImageSelector);
        }

        private void SaveChanges_OnClick(object sender, RoutedEventArgs e)
        {
            _implProject = new ProjectImpl();
            UpdateProjectData();
            int res = _implProject.Update(_project);
            if (res > 0)
            {
                _parentWindow.RefreshUserData();
                this.Close();
            }
            else
            {
                MessageBox.Show("Ha ocurrido un problema, inténtelo de nuevo");
            }
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void LoadProjectDataView()
        {
            TxbName.Text = _project.Name;
            TxbDescription.Text = _project.Description;
            CbxStatus.Text = _project.Status;
            LoadImagesInView();
            ImageSelectorGrid.Children.RemoveRange(0, ImageSelectorGrid.Children.Count - 1);
        }

        private void UpdateProjectData()
        {
            _project.Name = TxbName.Text;
            _project.Description = TxbDescription.Text;
            _project.Status = CbxStatus.Text;
            _project.ModifiedBy = AuthService.CurrentUserId;
        }

        private void DeleteProject_OnClick(object sender, RoutedEventArgs e)
        {
            _implProject = new ProjectImpl();
            int res = _implProject.Delete(_project);
            if (res > 0)
            {
                _parentWindow.RefreshUserData();
                this.Close();
            }
            else
            {
                MessageBox.Show("Ha ocurrido un problema, inténtelo de nuevo");
            }
        }

        private void ImageSelector_OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!(sender as ImageSelector).HasValue)
            {
                AddNewImageSelector();
            }
            else
            {
                ImageSelectorGrid.Children.Remove(sender as ImageSelector);
            }
        }

        private void UploadImages_OnClick(object sender, RoutedEventArgs e)
        {
            foreach (var imageSelectorChild in ImageSelectorGrid.Children)
            {
                if(((ImageSelector) imageSelectorChild).HasValue)
                    _uris.Add(((ImageSelector) imageSelectorChild).Uri);
            }

            Thread saveImagesProcess = new Thread(new ThreadStart(SaveImages));
            saveImagesProcess.Start();
        }

        private void SaveImages()
        {
            bool success = FileService.GetInstance().UploadProjectmages(_uris, _project.HappeningId);
            if (success)
            {
                MessageBox.Success("Se han subido las imágenes");
                ImagesGrid.Dispatcher.Invoke(LoadProjectDataView);
            }
            else
                MessageBox.Error("No se pudo subir todas las imágenes");
        }

        private void LoadImagesInView()
        {
            List<ProjectImage> images = GetProjectImages();
            ImagesGrid.Children.Clear();
            foreach (ProjectImage projectImage in images)
            {
                Uri resourceUri = new Uri(projectImage.Path, UriKind.Absolute);
                Image image = new Image();
                image.Source = new BitmapImage(resourceUri);
                image.Width = 130;
                image.Height = 130;

                System.Windows.Controls.ContextMenu contextMenu = new ContextMenu();
                MenuItem deleteOption = new MenuItem() {Header = "Eliminar imagen"};
                deleteOption.Click += DeleteImage_OnClick;
                deleteOption.Tag = projectImage.ImageId;
                contextMenu.Items.Add(deleteOption);
                image.ContextMenu = contextMenu;
                ImagesGrid.Children.Add(image);
            }
        }

        private void DeleteImage_OnClick(object sender, RoutedEventArgs e)
        {
            ProjectImageImpl projectImageImpl = new ProjectImageImpl();
            ProjectImage imageToDelete = new ProjectImage
            {
                ImageId = Int16.Parse(((MenuItem) sender).Tag.ToString()),
                ModifiedBy = AuthService.CurrentUserId
            };
            int res = projectImageImpl.Delete(imageToDelete);
            if (res > 0)
            {
                LoadProjectDataView();
            }
            else
            {
                MessageBox.Error("Hubo un error al eliminar la imagen.");
            }
        }

        private List<ProjectImage> GetProjectImages()
        {
            List<ProjectImage> images = new List<ProjectImage>();
            ProjectImageImpl projectImageImpl = new ProjectImageImpl();
            SqlDataReader imagesReader = projectImageImpl.Select(_project.HappeningId);
            while (imagesReader.Read())
            {
                ProjectImage projectImage = new ProjectImage();
                projectImage.ImageId = Int16.Parse(imagesReader[0].ToString());
                projectImage.Path = imagesReader[1].ToString();
                images.Add(projectImage);
            }
            imagesReader.Close();
            return images;
        }
    }
}