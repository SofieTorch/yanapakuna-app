﻿using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DAO.Implementation.TablesManipulation;
using DAO.Model;
using YanapakunaApp.Resources;
using YanapakunaApp.Resources.Interfaces;

namespace YanapakunaApp.Modules.Happenings
{
    public partial class EventsSubView : UserControl, IDataContainer
    {
        private EventImpl _implEvent;
        public event RefreshData RefreshEvent;
        public EventsSubView()
        {
            _implEvent = new EventImpl();
            RefreshEvent += LoadDataGrid;
            InitializeComponent();
        }

        private void AddNewEvent_OnClick(object sender, RoutedEventArgs e)
        {
            AddEventWindow window = new AddEventWindow(this);
            window.ShowDialog();
        }

        private void LoadDataGrid()
        {
            try
            {
                DgEvents.ItemsSource = null;
                DgEvents.ItemsSource = _implEvent.Select().DefaultView;
                DgEvents.Columns[0].Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void EventsSubView_OnLoaded(object sender, RoutedEventArgs e)
        {
            LoadDataGrid();
            Style rowStyle = new Style(typeof(DataGridRow));
            rowStyle.Setters.Add(new EventSetter(DataGridRow.MouseDoubleClickEvent,
                new MouseButtonEventHandler(OpenDetails_OnHandler)));
            DgEvents.RowStyle = rowStyle;
        }

        private void OpenDetails_OnHandler(object sender, MouseButtonEventArgs e)
        {
            EditEventWindow window = new EditEventWindow(this, GetSelectedEvent());
            window.ShowDialog();
        }

        private Event GetSelectedEvent()
        {
            DataRowView eventRow = (DataRowView) DgEvents.SelectedItems[0];
            Event @event = new Event();
            @event.HappeningId = Int16.Parse(eventRow[0].ToString());
            @event.Name = eventRow[1].ToString();
            @event.Description = eventRow[2].ToString();
            @event.StartDate = DateTime.Parse(eventRow[3].ToString());
            @event.FinishDate = DateTime.Parse(eventRow[4].ToString());

            return @event;
        }
        
        public void RefreshUserData()
        {
            RefreshEvent?.Invoke();
        }

        private void Search_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (SearchBar.Text.Length > 0)
            {
                try
                {
                    DgEvents.ItemsSource = null;
                    DgEvents.ItemsSource = _implEvent.Search(SearchBar.Text).DefaultView;
                    DgEvents.Columns[0].Visibility = Visibility.Collapsed;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }   
            }
            else LoadDataGrid();
        }
    }
}