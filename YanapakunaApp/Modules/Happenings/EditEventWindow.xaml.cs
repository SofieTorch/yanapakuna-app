﻿using System;
using System.Windows;
using DAO.Implementation.Services;
using DAO.Implementation.TablesManipulation;
using DAO.Model;
using YanapakunaApp.Resources.Interfaces;
using MessageBox = HandyControl.Controls.MessageBox;

namespace YanapakunaApp.Modules.Happenings
{
    public partial class EditEventWindow : Window
    {
        private IDataContainer _parentWindow;
        private EventImpl _implEvent;
        private Event _event;
        public EditEventWindow(IDataContainer parent, Event @event)
        {
            _parentWindow = parent;
            _event = @event;
            InitializeComponent();
            LoadEventDataView();
        }

        private void SaveChanges_OnClick(object sender, RoutedEventArgs e)
        {
            _implEvent = new EventImpl();
            UpdateEventData();
            int res = _implEvent.Update(_event);
            if (res > 0)
            {
                _parentWindow.RefreshUserData();
                MessageBox.Success("Se ha actualizado la información del evento");
                this.Close();
            }
            else
            {
                MessageBox.Error("No se ha podido actualizar la información el evento.");
            }
        }

        private void DeleteEvent_OnClick(object sender, RoutedEventArgs e)
        {
            _implEvent = new EventImpl();
            _event.ModifiedBy = AuthService.CurrentUserId;
            int res = _implEvent.Delete(_event);
            if (res > 0)
            {
                _parentWindow.RefreshUserData();
                MessageBox.Success("Se ha dado de baja el evento");
                this.Close();
            }
            else
            {
                MessageBox.Error("No se ha podido eliminar el evento.");
            }
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void UpdateEventData()
        {
            _event.Name = TxbName.Text;
            _event.Description = TxbDescription.Text;
            _event.StartDate = (DateTime) DpStartDate.SelectedDate;
            _event.FinishDate = (DateTime) DpFinishDate.SelectedDate;
            _event.ModifiedBy = AuthService.CurrentUserId;
        }

        private void LoadEventDataView()
        {
            TxbName.Text = _event.Name;
            TxbDescription.Text = _event.Description;
            DpStartDate.SelectedDate = _event.StartDate;
            DpFinishDate.SelectedDate = _event.FinishDate;
        }
    }
}