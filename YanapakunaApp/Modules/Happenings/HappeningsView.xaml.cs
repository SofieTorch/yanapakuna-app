﻿using System.Windows;
using System.Windows.Controls;

namespace YanapakunaApp.Modules.Happenings
{
    public partial class HappeningsView : UserControl
    {
        public HappeningsView()
        {
            InitializeComponent();
            GridContent.Children.Add(new ProjectsSubView());
        }

        private void ChangeHappeningView_OnCheck(object sender, RoutedEventArgs e)
        {
            GridContent.Children.Clear();
            UserControl content = null;
            
            switch (((RadioButton)sender).Content)
            {
                case "Proyectos":
                    content = new ProjectsSubView();
                    break;
                case "Eventos":
                    content = new EventsSubView();
                    break;
                default:
                    content = new UserControl();
                    break;
            }

            GridContent.Children.Add(content);   
        }
    }
}