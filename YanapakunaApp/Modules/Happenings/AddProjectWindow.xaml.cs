﻿using System.Windows;
using DAO.Implementation.Services;
using DAO.Implementation.TablesManipulation;
using DAO.Model;
using YanapakunaApp.Resources.Interfaces;

namespace YanapakunaApp.Modules.Happenings
{
    public partial class AddProjectWindow : Window
    {
        private IDataContainer _parentWindow;
        private ProjectImpl _implProject;
        private Project _project;
        public AddProjectWindow(IDataContainer parent)
        {
            _parentWindow = parent;
            InitializeComponent();
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void RegisterProject_OnClick(object sender, RoutedEventArgs e)
        {
            _implProject = new ProjectImpl();
            LoadProject();
            int res = _implProject.Insert(_project);
            if (res > 0)
            {
                _parentWindow.RefreshUserData();
                this.Close();
            }
            else
            {
                MessageBox.Show("Ha ocurrdo un error, inténtelo de nuevo.");
            }
        }

        private void LoadProject()
        {
            _project = new Project();
            _project.Name = TxbName.Text;
            _project.Description = TxbDescription.Text;
            _project.Status = CbxStatus.Text;
            _project.ModifiedBy = AuthService.CurrentUserId;
        }
    }
}