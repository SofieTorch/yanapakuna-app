﻿using System;
using System.Windows;
using DAO.Implementation.Services;
using DAO.Implementation.TablesManipulation;
using DAO.Model;
using YanapakunaApp.Resources.Interfaces;

namespace YanapakunaApp.Modules.Happenings
{
    public partial class AddEventWindow : Window
    {
        private EventImpl _implEvent;
        private IDataContainer _parentWindow;
        public AddEventWindow(IDataContainer parent)
        {
            _parentWindow = parent;
            InitializeComponent();
        }

        private void RegisterEvent_OnClick(object sender, RoutedEventArgs e)
        {
            _implEvent = new EventImpl();
            Event eventToRegister = GetEventToRegister();
            int res = _implEvent.Insert(eventToRegister);
            if (res > 0)
            {
                _parentWindow.RefreshUserData();
                this.Close();
            }
            else
            {
                MessageBox.Show("Ha ocurrido un problema, inténtelo de nuevo.");
            }
        }

        private void Cancel_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private Event GetEventToRegister()
        {
            Event eventToRegister = new Event();
            eventToRegister.Name = TxbName.Text;
            eventToRegister.Description = TxbDescription.Text;
            eventToRegister.StartDate = (DateTime) DpStartDate.SelectedDate;
            eventToRegister.FinishDate = (DateTime) DpFinishDate.SelectedDate;
            eventToRegister.ModifiedBy = AuthService.CurrentUserId;

            return eventToRegister;
        }
    }
}