﻿using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DAO.Implementation.TablesManipulation;
using DAO.Model;
using YanapakunaApp.Resources;
using YanapakunaApp.Resources.Interfaces;

namespace YanapakunaApp.Modules.Happenings
{
    public partial class ProjectsSubView : UserControl, IDataContainer
    {
        private ProjectImpl _implPoject;
        public event RefreshData RefreshEvent;
        public ProjectsSubView()
        {
            _implPoject = new ProjectImpl();
            RefreshEvent += LoadDataGrid;
            InitializeComponent();
        }

        private void AddNewProject_OnClick(object sender, RoutedEventArgs e)
        {
            AddProjectWindow window = new AddProjectWindow(this);
            window.ShowDialog();
        }

        private void LoadDataGrid()
        {
            try
            {
                DgProjects.ItemsSource = null;
                DgProjects.ItemsSource = _implPoject.Select().DefaultView;
                DgProjects.Columns[0].Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ProjectsSubView_OnLoaded(object sender, RoutedEventArgs e)
        {
            LoadDataGrid();
            Style rowStyle = new Style(typeof(DataGridRow));
                rowStyle.Setters.Add(new EventSetter(DataGridRow.MouseDoubleClickEvent,
                    new MouseButtonEventHandler(OpenDetails_OnHandler)));
                DgProjects.RowStyle = rowStyle;
        }

        private void OpenDetails_OnHandler(object sender, MouseButtonEventArgs e)
        {
            DataRowView projectRow = (DataRowView) DgProjects.SelectedItems[0];
            Project projectSelected = new Project();
            projectSelected.HappeningId = short.Parse(projectRow[0].ToString());
            projectSelected.Name = projectRow[1].ToString();
            projectSelected.Description = projectRow[2].ToString();
            projectSelected.Status = projectRow[3].ToString();

            EditProjectWindow window = new EditProjectWindow(this, projectSelected);
            window.ShowDialog();
        }
        
        public void RefreshUserData()
        {
            RefreshEvent?.Invoke();
        }

        private void Search_OnKeyUp(object sender, KeyEventArgs e)
        {
            if (SearchBar.Text.Length > 0)
            {
                try
                {
                    DgProjects.ItemsSource = null;
                    DgProjects.ItemsSource = _implPoject.Search(SearchBar.Text).DefaultView;
                    DgProjects.Columns[0].Visibility = Visibility.Collapsed;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }   
            }
            else LoadDataGrid();
        }
    }
}