﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace YanapakunaApp.Components
{
    public partial class TitleBar : UserControl
    {
        public TitleBar()
        {
            InitializeComponent();
        }

        public void DisableControlButtons()
        {
            BtnAbout.Visibility = Visibility.Collapsed;
            BtnMaximize.Visibility = Visibility.Collapsed;
            BtnRestore.Visibility = Visibility.Collapsed;
            BtnMinimize.Visibility = Visibility.Collapsed;
        }

        private void OnClickOpenAbout(object sender, RoutedEventArgs e)
        {
            Window win = new Window();
            win.Title = "Acerca";
            win.ResizeMode = ResizeMode.NoResize;
            win.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            win.WindowStyle = WindowStyle.None;
            win.Content = new AboutDialog();
            win.SizeToContent = SizeToContent.WidthAndHeight;
            win.ShowDialog();
        }

        private void OnClickCloseWindow(object sender, RoutedEventArgs e)
        {
            Window win = Window.GetWindow(this);
            win.Close();
        }

        private void OnClickMaximizeWindow(object sender, RoutedEventArgs e)
        {
            Window win = Window.GetWindow(this);
            win.WindowState = WindowState.Maximized;
            BtnMaximize.Visibility = Visibility.Collapsed;
            BtnRestore.Visibility = Visibility.Visible;
        }
        
        private void OnClickMinimizeWindow(object sender, RoutedEventArgs e)
        {
            Window win = Window.GetWindow(this);
            win.WindowState = WindowState.Minimized;
        }

        private void OnClickRestoreWindow(object sender, RoutedEventArgs e)
        {
            Window win = Window.GetWindow(this);
            win.WindowState = WindowState.Normal;
            BtnRestore.Visibility = Visibility.Collapsed;
            BtnMaximize.Visibility = Visibility.Visible;
        }
        
        private void OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var move = sender as System.Windows.Controls.Border;
            var win = Window.GetWindow(move);
            win.DragMove();
        }
    }
}