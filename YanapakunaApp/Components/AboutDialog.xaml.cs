﻿using System.Windows;
using System.Windows.Controls;

namespace YanapakunaApp.Components
{
    public partial class AboutDialog : UserControl
    {
        public AboutDialog()
        {
            InitializeComponent();
        }
        
        private void OnClickCloseWindow(object sender, RoutedEventArgs e)
        {
            Window win = Window.GetWindow(this);
            win.Close();
        }
    }
}