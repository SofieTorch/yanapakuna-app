﻿using System.Collections.Generic;
using DAO.Model;

namespace DAO.Interfaces
{
    public interface IEvent : ICrudBase<Event>
    {
        int AddSponsors(List<Company> sponsors);
        int AddStaff(List<Donor> staff);
    }
}