﻿using System.Data;
using DAO.Model;

namespace DAO.Interfaces
{
    public interface IVolunteer : ICrudBase<Volunteer>
    {
        DataTable SelectVolunteersWithoutUserAccount();
    }
}