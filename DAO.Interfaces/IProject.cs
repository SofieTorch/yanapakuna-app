﻿using System.Collections.Generic;
using DAO.Model;

namespace DAO.Interfaces
{
    public interface IProject : ICrudBase<Project>
    {
        int AddSponsors(List<Company> sponsors);
        int AddStaff(List<Donor> staff);
    }
}