﻿using DAO.Model;

namespace DAO.Interfaces
{
    public interface IContributor : ICrudBase<Contributor>
    {
        
    }
}