﻿using System.Data.SqlClient;
using DAO.Model;

namespace DAO.Interfaces
{
    public interface IProjectImage
    {
        SqlDataReader Select(short projectId);
        int Insert(ProjectImage image);
        int Delete(ProjectImage image);
        SqlDataReader GetImagesCount();
    }
}