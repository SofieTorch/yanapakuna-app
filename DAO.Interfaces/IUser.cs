﻿using System.Data;
using System.Data.SqlClient;
using DAO.Model;

namespace DAO.Interfaces
{
    public interface IUser : ICrudBase<User>
    {
        SqlDataReader VerifyIfUserNameExists(string userName);
        int RestorePassword(User t);
        int RestoreUserName(User t);
    }
}