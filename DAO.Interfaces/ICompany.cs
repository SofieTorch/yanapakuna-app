﻿using System.Data;
using DAO.Model;

namespace DAO.Interfaces
{
    public interface ICompany : ICrudBase<Company>
    {
        DataTable SelectCompanyById(short companyId);
    }
}