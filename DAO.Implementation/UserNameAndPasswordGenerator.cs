﻿using System;
using System.Text;

namespace DAO.Implementation
{
    public static class UserNameAndPasswordGenerator
    {
        public static string GenerateUserName(string name, string ci)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            name = name.TrimEnd();
            string[] words = name.Split(' ');
            builder.Append(random.Next(100, 999));

            foreach (string word in words)
            {
                if (word.Length > 0)
                {
                    builder.Append(word[0]);
                }
            }

            builder.Append(ci.Substring(0, 3));
            return builder.ToString();

        }

        public static string GeneratePassword()
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();

            builder.Append(GenerateRandomString(4, false));
            builder.Append(random.Next(1000, 9999));
            builder.Append(GenerateRandomString(3, true));

            return builder.ToString();
        }

        private static string GenerateRandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return lowerCase
                ? builder.ToString().ToLower()
                : builder.ToString();
        }
    }
}