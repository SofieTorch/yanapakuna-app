﻿using System.Collections.Generic;
using System.Data;
using DAO.Implementation.Services;
using DAO.Interfaces;
using DAO.Model;

namespace DAO.Implementation.TablesManipulation
{
    public class ProjectImpl : IProject
    {
        public int Insert(Project t)
        {
            string query = @"EXEC uspInsertProject @Name, @Description, @Status, @ModifiedBy";
            return DatabaseConnection.ExecuteQuery<int>(query, new object[]
            {
                t.Name, t.Description, t.Status, t.ModifiedBy
            });
        }

        public int Update(Project t)
        {
            string query = @"EXEC uspUpdateProject @ProjectId, @Name, @Description, @Status, @ModifiedBy";
            return DatabaseConnection.ExecuteQuery<int>(query, new object[]
            {
                t.HappeningId, t.Name, t.Description, t.Status, t.ModifiedBy
            });
        }

        public int Delete(Project t)
        {
            string query = @"EXEC uspDeleteProject @ProjectId, @ModifiedBy";
            return DatabaseConnection.ExecuteQuery<int>(query, new object[] {t.HappeningId, t.ModifiedBy});
        }

        public DataTable Select()
        {
            string query = @"SELECT * FROM vwProjects";
            return DatabaseConnection.ExecuteQuery<DataTable>(query, new object[0]);
        }

        public DataTable Search(string criteria)
        {
            string query = @"SELECT * FROM vwProjects
                            WHERE LOWER(Proyecto) LIKE CONCAT('%', @Criteria, '%')";
            return DatabaseConnection.ExecuteQuery<DataTable>(query, new[] {criteria});
        }
        
        public int AddSponsors(List<Company> sponsors)
        {
            return 0;
        }

        public int AddStaff(List<Donor> staff)
        {
            return 0;
        }
    }
}