﻿using System.Data.SqlClient;
using DAO.Implementation.Services;
using DAO.Interfaces;
using DAO.Model;

namespace DAO.Implementation.TablesManipulation
{
    public class ProjectImageImpl : IProjectImage
    {
        public SqlDataReader Select(short projectId)
        {
            string query = @"SELECT ImageID, Path FROM ProjectImage
                            WHERE ProjectID = @ProjectId AND RecordStatus = 1";
            return DatabaseConnection.ExecuteQuery<SqlDataReader>(query, new object[]{projectId});
        }

        public int Insert(ProjectImage image)
        {
            string query = @"
                    INSERT INTO ProjectImage (Path, ProjectID, ModifiedBy, LastUpdateDate)
                    VALUES (@Path, @ProjectId, @ModifiedBy, CAST(SYSDATETIME() AS datetime))";
            return DatabaseConnection.ExecuteQuery<int>(query, new object[]
            {
                image.Path, image.ProjectId, image.ModifiedBy
            });
        }

        public int Delete(ProjectImage image)
        {
            string query = @"
                    UPDATE ProjectImage SET
                        RecordStatus = 0,
                        ModifiedBy = @ModifiedBy,
                        LastUpdateDate = CAST(SYSDATETIME() AS datetime)
                    WHERE ImageID = @ImageId";
            return DatabaseConnection.ExecuteQuery<int>(query, new object[]
            {
                image.ModifiedBy, image.ImageId
            });
        }

        public SqlDataReader GetImagesCount()
        {
            string query = "SELECT MAX(ImageID) FROM ProjectImage";
            return DatabaseConnection.ExecuteQuery<SqlDataReader>(query, new object[0]);
        }
    }
}