﻿using System;
using System.Data;
using System.Data.SqlClient;
using DAO.Implementation.Services;
using DAO.Interfaces;
using DAO.Model;

namespace DAO.Implementation.TablesManipulation
{
    public class UserImpl : IUser
    {
        public int Insert(User t)
        {
            string password = UserNameAndPasswordGenerator.GeneratePassword();
            string query = @"
                INSERT INTO [User](UserID, UserName, Password, Role, Email, ModifiedBy, LastUpdateDate)
                VALUES (@userId, @userName, HASHBYTES('md5', @password), @role, @email, @modifiedBy, CURRENT_TIMESTAMP)";
            
            try
            {
                SqlCommand command = DatabaseConnection.CreateCommand(query);
                command.Parameters.AddWithValue("@userId", t.UserId);
                command.Parameters.AddWithValue("@userName", t.UserName);
                command.Parameters.AddWithValue("@password", password).SqlDbType = SqlDbType.VarChar;
                command.Parameters.AddWithValue("@role", t.UserRole.ToString());
                command.Parameters.AddWithValue("@email", t.Email);
                command.Parameters.AddWithValue("@modifiedBy", t.ModifiedBy);

                EmailSender emailSender = new EmailSender(t.Email, t.Name, "Datos de aceso a Yanapakuna",
                    $"Te acaban de registrar en Yanapakuna! Ahora puedes acceder al sistema, estos son tus datos:\n\n" +
                    $"Nombre se usuario: {t.UserName}\n" +
                    $"Contraseña: {password}\n\n" +
                    "Que Tengas un buen día!");
                emailSender.SendEmail();
                
                return DatabaseConnection.ExecuteModificationCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }

        public int Update(User t)
        {
            string query = @"
                UPDATE [User] SET
                    UserName = @UserName,
                    Role = @Role, Email = @Email,
                    LastUpdateDate = CAST(SYSDATETIME() AS datetime),
                    ModifiedBy = @ModifiedBy
                WHERE UserID = @UserId";
            
            try
            {
                return DatabaseConnection.ExecuteQuery<int>(query, 
                    new string[]
                    { t.UserName, t.UserRole.ToString(), t.Email,
                    t.ModifiedBy.ToString(), t.UserId.ToString() }
                );
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Delete(User t)
        {
            string query = @"UPDATE [User] SET RecordStatus = 0,
                                ModifiedBy = @ModifiedBy
                            WHERE UserID = @UserId";
            try
            {
                return DatabaseConnection.ExecuteQuery<int>(query, 
                    new string[] { t.ModifiedBy.ToString(), t.UserId.ToString() }
                );
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Select()
        {
            string query = @"SELECT * FROM vwUser";
            try
            {
                return DatabaseConnection.ExecuteQuery<DataTable>(query, new string[0]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SqlDataReader VerifyIfUserNameExists(string userName)
        {
            string query = @"
                IF @UserName IN (SELECT UserName FROM [User])
                     SELECT 1
                ELSE SELECT 0";
            try
            {
                return DatabaseConnection.ExecuteQuery<SqlDataReader>(query, new []{ userName });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int RestorePassword(User t)
        {
            string password = UserNameAndPasswordGenerator.GeneratePassword();
            string query = @"
                UPDATE [User] SET Password = HASHBYTES('md5', @password),
                  ModifiedBy = @modifiedBy,
                  PasswordUpdateReq = 1,
                  LastUpdateDate = CAST(SYSDATETIME() AS datetime)
                WHERE UserID = @userId";
            
            try
            {
                SqlCommand command = DatabaseConnection.CreateCommand(query);
                command.Parameters.AddWithValue("@userId", t.UserId);
                command.Parameters.AddWithValue("@password", password).SqlDbType = SqlDbType.VarChar;
                command.Parameters.AddWithValue("@modifiedBy", t.ModifiedBy);

                EmailSender emailSender = new EmailSender(t.Email, t.Name, "Datos de aceso a Yanapakuna",
                    $"Tu cuenta ha sido actualizada con una nueva contraseña, ahora puedes ingresar con los siguientes datos:\n\n" +
                    $"Nueva contraseña: {password}\nNo olvides cambiarla cuando inicies sesión!\n\n" +
                    "Por seguridad no compartas esta información con nadie.\nQue Tengas un buen día!");
                emailSender.SendEmail();
                
                return DatabaseConnection.ExecuteModificationCommand(command);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int RestoreUserName(User t)
        {
            string query = @"
                UPDATE [User] SET UserName = @UserName,
                  ModifiedBy = @ModifiedBy,
                  LastUpdateDate = CAST(SYSDATETIME() AS datetime)
                WHERE UserID = @UserId";
            
            try
            {
                int response = DatabaseConnection.ExecuteQuery<int>(query,
                    new[] {t.UserName, t.ModifiedBy.ToString(), t.UserId.ToString()});

                EmailSender emailSender = new EmailSender(t.Email, t.Name, "Datos de aceso a Yanapakuna",
                    $"Tu cuenta ha sido actualizada con un nuevo nombre se usuario, ahora puedes ingresar con los siguientes datos:\n\n" +
                    $"Nuevo nombre de usuario: {t.UserName}\n\n" +
                    "Por seguridad no compartas esta información con nadie.\nQue Tengas un buen día!");
                emailSender.SendEmail();
                
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}