﻿using System.Collections.Generic;
using System.Data;
using DAO.Implementation.Services;
using DAO.Interfaces;
using DAO.Model;

namespace DAO.Implementation.TablesManipulation
{
    public class EventImpl : IEvent
    {
        public int Insert(Event t)
        {
            string query = @"EXEC uspInsertEvent @Name, @Description, @StartDate, @FinishDate, @ModifiedBy";
            return DatabaseConnection.ExecuteQuery<int>(query, new object[]
            {
                t.Name, t.Description, t.StartDate.Date, t.FinishDate.Date, t.ModifiedBy
            });
        }

        public int Update(Event t)
        {
            string query = @"EXEC uspUpdateEvent @EventId, @Name, @Description, @StartDate, @FinishDate, @ModifiedBy";
            return DatabaseConnection.ExecuteQuery<int>(query, new object[]
            {
                t.HappeningId, t.Name, t.Description, t.StartDate, t.FinishDate, t.ModifiedBy
            });
        }

        public int Delete(Event t)
        {
            string query = @"EXEC uspDeleteEvent @EventId, @ModifiedBy";
            return DatabaseConnection.ExecuteQuery<int>(query, new object[] {t.HappeningId, t.ModifiedBy});
        }

        public DataTable Select()
        {
            string query = @"SELECT * FROM vwEvent";
            return DatabaseConnection.ExecuteQuery<DataTable>(query, new object[0]);
        }

        public DataTable Search(string criteria)
        {
            string query = @"SELECT * FROM vwEvent
                            WHERE LOWER(Evento) LIKE CONCAT('%', @Criteria, '%')";
            return DatabaseConnection.ExecuteQuery<DataTable>(query, new[] {criteria});
        }

        public int AddSponsors(List<Company> sponsors)
        {
            return 0;
        }

        public int AddStaff(List<Donor> staff)
        {
            return 0;
        }
    }
}