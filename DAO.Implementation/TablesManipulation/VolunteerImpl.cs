﻿using System.Data;
using DAO.Implementation.Services;
using DAO.Interfaces;
using DAO.Model;

namespace DAO.Implementation.TablesManipulation
{
    public class VolunteerImpl : IVolunteer
    {
        public int Insert(Volunteer t)
        {
            string query = @"EXEC uspInsertVolunteer @Name, @PaternalSurname, @MaternalSurname, @Ci, @ModifiedBy";
            return DatabaseConnection.ExecuteQuery<int>(query, new object[]
            {
                t.Name, t.PaternalSurname, t.MaternalSurname, t.NitOrCi, t.ModifiedBy
            });
        }

        public int Update(Volunteer t)
        {
            string query = @"EXEC uspUpdateVolunteer @VolunteerId, @Name, @PaternalSurname,
                            @MaternalSurname, @Ci, @VolunteeringStatus, @ModifiedBy";
            return DatabaseConnection.ExecuteQuery<int>(query, new object[]
            {
                t.DonorId, t.Name, t.PaternalSurname, t.MaternalSurname,
                t.NitOrCi, t.VolunteeringStatus, t.ModifiedBy
            });
        }

        public int Delete(Volunteer t)
        {
            string query = @"EXEC uspDeleteVolunteer @VolunteerId, @ModifiedBy";
            return DatabaseConnection.ExecuteQuery<int>(query, new object[]
            {
                t.DonorId, t.ModifiedBy
            });
        }

        public DataTable Select()
        {
            string query = @"SELECT * FROM vwVolunteer";
            return DatabaseConnection.ExecuteQuery<DataTable>(query, new object[0]);
        }

        public DataTable SelectVolunteersWithoutUserAccount()
        {
            string query = @"SELECT * FROM vwVolunteersWithoutUserAccount";
            return DatabaseConnection.ExecuteQuery<DataTable>(query, new object[0]);
        }

        public DataTable Search(string criteria)
        {
            string query = @"SELECT * FROM vwVolunteer
                            WHERE LOWER(CONCAT(Nombre, ' ', [Apellido paterno], ' ', [Apellido materno], ' ', CI))
                                      LIKE CONCAT('%', @Criteria, '%')";
            return DatabaseConnection.ExecuteQuery<DataTable>(query, new object[] {criteria});
        }
    }
}