﻿using System.Data;
using DAO.Implementation.Services;
using DAO.Interfaces;
using DAO.Model;

namespace DAO.Implementation.TablesManipulation
{
    public class ContributorImpl : IContributor
    {
        public int Insert(Contributor t)
        {
            string query = @"EXEC uspInsertContributor @Name, @PaternalSurname, @MaternalSurname, @Ci, @ModifiedBy";
            return DatabaseConnection.ExecuteQuery<int>(query, new object[]
            {
                t.Name, t.PaternalSurname, t.MaternalSurname, t.NitOrCi, t.ModifiedBy
            });
        }

        public int Update(Contributor t)
        {
            string query = @"uspUpdateContributor @ContributorId, @Name, @PaternalSurname, @MaternalSurname, @Ci, @ModifiedBy";
            return DatabaseConnection.ExecuteQuery<int>(query, new object[]
            {
                t.DonorId, t.Name, t.PaternalSurname, t.MaternalSurname, t.NitOrCi, t.ModifiedBy
            });
        }

        public int Delete(Contributor t)
        {
            string query = @"uspDeleteContributor @ContributorId, @ModifiedBy";
            return DatabaseConnection.ExecuteQuery<int>(query, new object[] { t.DonorId, t.ModifiedBy });
        }

        public DataTable Select()
        {
            string query = @"SELECT * FROM vwContributor";
            return DatabaseConnection.ExecuteQuery<DataTable>(query, new object[0]);
        }

        public DataTable Search(string criteria)
        {
            string query = @"SELECT * FROM vwContributor
                            WHERE LOWER(CONCAT(Nombre, ' ', [Apellido paterno], ' ', [Apellido materno], ' ', CI))
                                      LIKE CONCAT('%', @Criteria, '%')";
            return DatabaseConnection.ExecuteQuery<DataTable>(query, new object[] {criteria});
        }
    }
}