﻿using System.Data;
using DAO.Implementation.Services;
using DAO.Interfaces;
using DAO.Model;

namespace DAO.Implementation.TablesManipulation
{
    public class CompanyImpl : ICompany
    {
        public int Insert(Company t)
        {
            string query = @"
                EXEC uspInsertCompany @Name, @Nit, @Description,
                    @AllianceType, @ContactName, @ContactPhone, @ModifiedBy";

            return DatabaseConnection.ExecuteQuery<int>(query, new object[]
            {
                t.Name, t.NitOrCi, t.Description, t.AllianceType,
                t.ContactName, t.ContactPhone, t.ModifiedBy
            });
        }

        public int Update(Company t)
        {
            string query = @"
                EXEC uspUpdateCompany @CompanyId, @Name, @Nit, @Description,
                    @AllianceType, @AllianceActive, @ContactName, @ContactPhone, @ModifiedBy";
            return DatabaseConnection.ExecuteQuery<int>(query, new object[]
            {
                t.DonorId, t.Name, t.NitOrCi, t.Description, t.AllianceType,
                t.AllianceStatus, t.ContactName, t.ContactPhone, t.ModifiedBy
            });
        }

        public int Delete(Company t)
        {
            string query = @"EXEC uspDeleteCompany @CompanyId, @ModifiedBy";
            return DatabaseConnection.ExecuteQuery<int>(query, new object[] {t.DonorId, t.ModifiedBy});
        }

        public DataTable Select()
        {
            string query = @"SELECT * FROM vwCompany";
            return DatabaseConnection.ExecuteQuery<DataTable>(query, new object[0]);
        }

        public DataTable SelectCompanyById(short companyId)
        {
            string query = @"
                SELECT C.CompanyID,
                       D.Name AS 'Nombre',
                       D.NitOrCi AS 'Nit',
                       C.Description AS 'Descripción',
                       C.ContactName AS 'Contacto',
                       C.ContactPhone AS 'Número de contacto',
                       D.ActuallyActive AS 'Alianza activa',
                       C.AllianceType AS 'Tipo de alianza'
                FROM Donor D
                         INNER JOIN Company C ON D.DonorID = C.CompanyID
                WHERE C.RecordStatus = 1 AND C.CompanyID = @CompanyId";
            
            return DatabaseConnection.ExecuteQuery<DataTable>(query, new[] { companyId.ToString() });
        }

        public DataTable Search(string criteria)
        {
            string query = @"SELECT * FROM vwCompany
                            WHERE LOWER(CONCAT(Nombre, Nit))
                            LIKE CONCAT('%', @Criteria, '%')";
            return DatabaseConnection.ExecuteQuery<DataTable>(query, new[] {criteria});
        }
    }
}