﻿using System.Net;
using System.Net.Mail;

namespace DAO.Implementation.Services
{
    public class EmailSender
    {
        private string _fromAddress;
        private string _fromPassword;
        private string _fromName;
        private string _toAddress;
        private string _toName;
        private string _subject;
        private string _message;
        
        public EmailSender(string toAddress, string toName, string subject, string message)
        {
            _fromAddress = "soff.torch@gmail.com";
            _fromPassword = "Hemmo2828";
            _fromName = "Sofia de Yanapakuna";
            _toAddress = toAddress;
            _toName = toName;
            _subject = subject;
            _message = message;
        }

        public void SendEmail()
        {
            var fromAddress = new MailAddress(_fromAddress, _fromName);
            var toAddress = new MailAddress(_toAddress, _toName);

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, _fromPassword)
            };

            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = _subject,
                Body = _message

            })
            {
                smtp.Send(message);
            }
        }
    }
}