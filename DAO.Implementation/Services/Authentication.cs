﻿using System;
using System.Data;
using System.Data.SqlClient;
using DAO.Model;

namespace DAO.Implementation.Services
{
    public class AuthService
    {
        public static short CurrentUserId { get; set; }
        public static string CurrentUserName { get; set; }
        public static string CurrentUserEmail { get; set; }
        public static UserRole? CurrentUserRole { get; set; }
        
        private DataTable GetUserAsTable(string userName, string password)
        {
            string query = @"
                SELECT UserID, UserName, Role, Email, PasswordUpdateReq
                FROM [User]
                WHERE RecordStatus = 1 AND UserName = @userName AND Password = HASHBYTES('md5', @password)";
            
            SqlCommand command = DatabaseConnection.CreateCommand(query);
            command.Parameters.AddWithValue("@userName", userName);
            command.Parameters.AddWithValue("@password", password).SqlDbType = SqlDbType.VarChar;
            return DatabaseConnection.ExecuteSelectCommand(command);
        }

        public User LogIn(string userName, string password, bool updateCurrentUser)
        {
            User currentUser = null;
            DataTable userAsTable = GetUserAsTable(userName, password);
            bool userExists = userAsTable.Rows.Count == 1;
            
            if (userExists)
            {
                currentUser = new User(
                    Int16.Parse(userAsTable.Rows[0][0].ToString()),
                    userAsTable.Rows[0][1].ToString(),
                    (UserRole) Enum.Parse(typeof(UserRole), userAsTable.Rows[0][2].ToString()),
                    userAsTable.Rows[0][3].ToString(),
                    Convert.ToByte(userAsTable.Rows[0][4].ToString())
                );

                if (updateCurrentUser)
                {
                    CurrentUserId = currentUser.UserId;
                    CurrentUserName = currentUser.UserName;
                    CurrentUserEmail = currentUser.Email;
                    CurrentUserRole = currentUser.UserRole;
                    System.Diagnostics.Debug.WriteLine(
                        $"[{DateTime.Now}] | Usuario {CurrentUserName}, ha iniciado sesión");
                }
            }

            return currentUser;
        }

        public int UpdatePassword(string newPassword)
        {
            string query = @"
                UPDATE [User] SET Password = HASHBYTES('md5', @password),
                  ModifiedBy = @modifiedBy,
                  PasswordUpdateReq = 0,
                  LastUpdateDate = CAST(SYSDATETIME() AS datetime)
                WHERE UserID = @userId";

            SqlCommand command = DatabaseConnection.CreateCommand(query);
            command.Parameters.AddWithValue("@password", newPassword).SqlDbType = SqlDbType.VarChar;
            command.Parameters.AddWithValue("@modifiedBy", CurrentUserId);
            command.Parameters.AddWithValue("@userId", CurrentUserId);
            return DatabaseConnection.ExecuteModificationCommand(command);
        }
        
        public void LogOut()
        {
            CurrentUserId = -1;
            CurrentUserName = null;
            CurrentUserEmail = null;
            CurrentUserRole = null;
            System.Diagnostics.Debug.WriteLine(
                $"[{DateTime.Now}] | Usuario {CurrentUserName}, ha cerrado sesión");
        }
    }
}