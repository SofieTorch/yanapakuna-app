﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using DAO.Implementation.TablesManipulation;
using DAO.Model;

namespace DAO.Implementation.Services
{
    public class FileService
    {
        private static FileService _instance;
        private const string ProjectImagesPath = @"D:\Users\sofia\Pictures\Yanapakuna\Projects";

        private ProjectImageImpl _projectImageImpl;

        private FileService()
        {
            _projectImageImpl = new ProjectImageImpl();
        }

        public static FileService GetInstance()
        {
            if (_instance == null)
            {
                _instance = new FileService();
            }

            return _instance;
        }

        public bool UploadProjectmages(List<Uri> uris, short projectId)
        {
            foreach (Uri uri in uris)
            {
                int dotIntex = uri.AbsolutePath.LastIndexOf(".");
                string extension = uri.AbsolutePath.Substring(dotIntex);
                string fileName = GetNextNameForProjectImage() + extension;
                
                if (!CopyFile(uri.AbsolutePath, ProjectImagesPath, fileName))
                    return false;

                string newImageFullPath = $"{ProjectImagesPath}\\{fileName}";
                ProjectImage projectImage = new ProjectImage(newImageFullPath, projectId);
                projectImage.ModifiedBy = AuthService.CurrentUserId;
                int res = _projectImageImpl.Insert(projectImage);
                if (res == 0) return false;
            }

            return true;
        }

        private string GetNextNameForProjectImage()
        {
            SqlDataReader countReader = _projectImageImpl.GetImagesCount();
            countReader.Read();
            string name = (Int16.Parse(countReader[0].ToString()) + 1).ToString();
            countReader.Close();
            return name;
        }

        private bool CopyFile(string pathSource, string pathDestination, string fileName)
        {
            try
            {
                File.Copy(pathSource, Path.Combine(pathDestination, fileName), true);
            }
            catch (Exception e)
            {
                throw;
                return false;
            }

            return true;
        }
    }
}