﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;

namespace DAO.Implementation.Services
{
    public class DatabaseConnection
    {
        private static string databaseUri =
            "data source = localhost; initial catalog = dbYanapakuna; user id = sa; password = Hemmo1996";

        public static T ExecuteQuery<T>(string query, object[] paramsValues)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine(
                    $"[{DateTime.Now}] | Usuario: {AuthService.CurrentUserName} ejecutando la peticion {query}...");
                SqlCommand command = CreateCommand(query);
                List<string> parameters = GetQueryParameters(query);
                for (int i = 0; i < parameters.Count; i++)
                    command.Parameters.AddWithValue(parameters[i], paramsValues[i]);

                System.Diagnostics.Debug.WriteLine(
                    $"[{DateTime.Now}] | Usuario {AuthService.CurrentUserName}, ejecucion correcta de la peticion {query}");
                return ExecuteCommand<T>(command);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(
                    $"[{DateTime.Now}] | ERROR : Usuario {AuthService.CurrentUserName}, {ex.Message}");
                var @switch = new Dictionary<Type, Func<SqlCommand, T>>
                {
                    { typeof(DataTable), (SqlCommand com) => (T) Convert.ChangeType(new DataTable(), typeof(T)) },
                    { typeof(SqlDataReader), (SqlCommand com) => (T) Convert.ChangeType(null, typeof(T)) },
                    { typeof(int), (SqlCommand com) => (T) Convert.ChangeType(0, typeof(T))}
                };

                return @switch[typeof(T)](new SqlCommand());
            }
        }
        
        private static T ExecuteCommand<T>(SqlCommand command)
        {
            var @switch = new Dictionary<Type, Func<SqlCommand, T>>
            {
                { typeof(DataTable), (SqlCommand com) => (T) Convert.ChangeType(ExecuteSelectCommand(com), typeof(T)) },
                { typeof(SqlDataReader), (SqlCommand com) => (T) Convert.ChangeType(ExecuteDataReaderCommand(com), typeof(T)) },
                { typeof(int), (SqlCommand com) => (T) Convert.ChangeType(ExecuteModificationCommand(com), typeof(T))}
            };

            return @switch[typeof(T)](command);
        }

        private static List<string> GetQueryParameters(string query)
        {
            // replacing non-alphanumeric characters for spaces, except for "@"
            string queryClean = Regex.Replace(query, "[^a-zA-Z0-9@]", " ");
            // replacing more than one spaces for only one
            queryClean = Regex.Replace(queryClean, "  +", " ");
            string[] cleanQueryElements = queryClean.Split(' ');
            // is a parameter if starts with "@" and the parameter name starts with Upper case
            IEnumerable<string> parameters = cleanQueryElements.Where(word => word.Length > 0 && word[0] == '@' && Char.IsUpper(word[1]));
            return parameters.ToList();
        }
        
        public static SqlCommand CreateCommand(string query)
        {
            SqlConnection connection = new SqlConnection(databaseUri);
            SqlCommand command = new SqlCommand(query);
            command.Connection = connection;
            return command;
        }

        /// <summary>
        /// Executes a command for Insert, Update and Delete operations
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public static int ExecuteModificationCommand(SqlCommand command)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine(
                    $"[{DateTime.Now}] | Usuario {AuthService.CurrentUserName}, ejecutando query por un int, modificando base de datos...");
                command.Connection.Open();
                System.Diagnostics.Debug.WriteLine(
                    $"[{DateTime.Now}] | Usuario {AuthService.CurrentUserName}, ejecucion correcta del query por un int, base de datos modificada");
                return command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(
                    $"[{DateTime.Now}] | ERROR : Usuario {AuthService.CurrentUserName}, {ex.Message}");
                return 0;
            }
            finally
            {
                command.Connection.Close();
            }
        }

        /// <summary>
        /// Executes a command for Select operations which includes more than 1 row
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public static DataTable ExecuteSelectCommand(SqlCommand command)
        {
            DataTable res = new DataTable();
            try
            {
                System.Diagnostics.Debug.WriteLine(
                    $"[{DateTime.Now}] | Usuario {AuthService.CurrentUserName}, ejecutando query por un DataTable");
                command.Connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(res);
                System.Diagnostics.Debug.WriteLine(
                    $"[{DateTime.Now}] | Usuario {AuthService.CurrentUserName}, ejecucion correcta del query por un DataTable");
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(
                    $"[{DateTime.Now}] | ERROR : Usuario {AuthService.CurrentUserName}, {ex.Message}");
            }
            finally
            {
                command.Connection.Close();
            }

            return res;
        }

        private static SqlDataReader ExecuteDataReaderCommand(SqlCommand command)
        {
            SqlDataReader dr = null;
            try
            {
                System.Diagnostics.Debug.WriteLine(
                    $"[{DateTime.Now}] | Usuario {AuthService.CurrentUserName}, ejecutando query por un SqlDataReader");
                command.Connection.Open();
                dr = command.ExecuteReader();
                System.Diagnostics.Debug.WriteLine(
                    $"[{DateTime.Now}] | Usuario {AuthService.CurrentUserName}, ejecucion correcta del query por un SqlDataReader");
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(
                    $"[{DateTime.Now}] | ERROR : Usuario {AuthService.CurrentUserName}, {e.Message}");
            }

            return dr;
        }
    }
}
